class CreateOrderItemDto {
  customerId: number;
  amount: number;
  productId: number;
}
export class CreateOrderDto {
  customerId: number;
  orderItems: CreateOrderItemDto[];
}
